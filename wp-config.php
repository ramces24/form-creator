<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_from_scratch');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xspa9mCs!E,@#{}#Q,Gx[(2+XbPXSlvG^J-$^/0!KF1nnNUDTRo.<=q8=p~t]$WW');
define('SECURE_AUTH_KEY',  ')_WJQz~~C:vJbrO(T^,1AQq0.45KrVUZ88C^uZQZ!0/VNiR9WoC-_f2 W`*9Y-S6');
define('LOGGED_IN_KEY',    '5*&Ob8V6KiIz<J0%:#;St[y,X__?;jt;> X4ajFhnN`j(1jNn#1Sa&k%22BMC_J.');
define('NONCE_KEY',        'PNN$Igy@OQMyNabV,dteQB8.A%0RyQb7b3Oi0WB,@lR~GZ]xUWs{E?TYABL.0nKZ');
define('AUTH_SALT',        'o?sko$q^B<hXx>Rjcdu8rSwLouP@e*IM0iDLx$!xh Wn4*FcOA=/@<c.C1xO?3c!');
define('SECURE_AUTH_SALT', 'I+1(_v$_CO!(,+-bhy;srZEAA33QjkVeGFnK`~wzfim|I0K|]Z rEDsS6~|d|R(^');
define('LOGGED_IN_SALT',   'n,<oRmYilP &43&l9sY}SsOI{PE1^rg0N+tE6~XxRK.*>BD[m%X:Dv@@#eK8oz8 ');
define('NONCE_SALT',       'Lwt>%H 7ju&cVd6pcM{o_ji0PZ.^TIkjHb]fXejX2q; dt_{-zn 803{,Q{}h(dj');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
