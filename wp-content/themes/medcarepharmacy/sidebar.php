<aside class="float-left page-sidebar">
	<div class="services">
			<div class="icon">
					<img src="images/services-note.png" alt="">
			</div>
			<div class="service-description">
					<h4>SERVICES <span>WE OFFER</span></h4>
			</div>
	</div>
	<div class="btns">
		<ul>
			<li><a href="#">FREE delivery</a></li>
			<li><a href="#">Disease Information</a></li>
			<li><a href="#">Health Resources</a></li>
			<li><a href="#">Drug Infromation</a></li>
			<li><a href="#">Natural Medicine</a></li>
			<li><a href="#">View More Services</a></li>
		</ul>
	</div>
</aside>
