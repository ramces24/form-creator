<?php get_header(); ?>
  <div id="banner">
    <?php get_template_part('template-parts/content','banner'); ?>
  </div>
  <div id="mid">
    <?php get_template_part('template-parts/content','mid'); ?>
  </div>
  <div id="featured-product">
    <?php get_template_part('template-parts/content','featuredproducts'); ?>
  </div>
  <div class="main container clearfix">
    <div class="text-align-center">
        <div class="sidebar">
            <?php get_sidebar(); ?>
        </div>
      </aside>
    </div>
  </div>
<?php get_footer(); ?>
