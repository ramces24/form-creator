
		<div class="container clearfix margin-top">
			<div class="flash2">
					<ul class="rslides">
						<li><img src="images/slides/slide5.png" alt=""></li>
						<li><img src="images/slides/slide6.png" alt=""></li>
						<li><img src="images/slides/slide7.png" alt=""></li>
					</ul>
					<div class="slider-desc">
							<div class="slider-desc-title">
									<h1>Your Partner <span>HEALTH</span></h1>
							</div>
							<p>Non aliqua in officia exercitation tempor aliquip lorem ex lorem. Do eiusmod sunt ad incididunt voluptate ipsum ut elit. Non ea et, ut velit commodo sed.</p>
							<a href="/about_us" class="banner-button-responsive">Read More</a>
					</div>
			</div>
			<div class="positioning">
					<div class="banner-left">
							<div class="flash">
								<div class="box_skitter box_skitter_large">
									<ul>
										<?php
												$galleryIds = get_post_gallery_ids($post->ID);
										 ?>
										 <?php foreach ($galleryIds as $data): ?>
											 	<li><img src="<?php echo wp_get_attachment_url($data); ?>" alt="" class="random"></li>
										 <?php endforeach; ?>
									</ul>
								</div>
							</div>
					</div>
					<div class="banner-right">
							<div class="banner-right-container">
									<div class="banner-title">
											<h1><?php the_field('field_5901ae4ad16d5'); ?></h1>
											<div class="flower-border"></div>
									</div>
									<div class="banner-desc">
											<?php the_field('field_5901b23dc37b0'); ?>
									</div>
									<a href="<?php echo get_permalink(23) ?>" class="banner-button"> Learn More</a>
							</div>
					</div>
			</div>
		</div>
