
  <div class="container">
      <div class="feature-products-title">
          <h4>Featured Products</h4>
      </div>
      <div class="featured-product-list">
          <?php
                $featured_query = new WP_Query( array(
                      'tax_query' => array(
                              array(
                                  'taxonomy' => 'product_visibility',
                                  'field'    => 'name',
                                  'terms'    => 'featured',
                                  'operator' => 'IN'
                              ),
                       ),
                       'posts_per_page' => 5
                  ) );
                if($featured_query->have_posts()):
                    while($featured_query->have_posts()):
                      $featured_query->the_post();
          ?>
                      <div class="featured-box">
                          <div class="featured-body">
                              <div class="product-title">
                                    <h5><?php the_title(); ?></h5>
                              </div>
                              <div class="product-img">
                                    <img src="<?php $url =  wp_get_attachment_image_src( get_post_thumbnail_id($post->ID));echo $url[0];  ?>" alt="">
                              </div>
                              <div class="product-price">
                                <span><?php $price = get_post_meta( get_the_ID(), '_regular_price', true);echo $price ?></span>
                              </div>
                              <div class="product-description">
                                  <p><?php the_excerpt(); ?></p>
                              </div>
                              <div class="product-cart-btns">
                                  <a href="shop/?add-to-cart=<?php echo get_the_ID(); ?>" class="cart-btn"><span></span></a><?php $permalink = get_permalink(get_the_ID()); ?> <a href="<?php echo $permalink ?>" class="cart-details"><span>Details</span></a>
                              </div>
                          </div>
                      </div>
          <?php
                    endwhile;
                else:
                  echo "no featured";
                endif;
                wp_reset_query();
           ?>
      </div>
  </div>
