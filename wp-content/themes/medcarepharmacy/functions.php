<?php
  function include_scripts_styles(){
    wp_enqueue_style( 'normalize3', get_template_directory_uri() . '/css/assets/normalize.css', array(), '3.2' );
    wp_enqueue_style( 'helper1', get_template_directory_uri() . '/css/assets/helper.css', array(), '3.2' );
    wp_enqueue_style( 'responsiveslides4', get_template_directory_uri() . '/css/assets/responsiveslides.css', array(), '3.2' );
    wp_enqueue_style( 'templatestylesheet5',get_stylesheet_uri() );
    wp_enqueue_style( 'mediaquery2', get_template_directory_uri() . '/css/assets/media.css', array(), '3.2' );
    wp_enqueue_style( 'skitter6', get_template_directory_uri() . '/css/assets/skitter.styles.css', array(), '3.2' );

    wp_enqueue_script( 'modernizr5', get_template_directory_uri() . '/js/assets/vendor/modernizr-custom-v2.8.3.min.js', array(), '20141010', true );
    wp_enqueue_script( 'selectiviz7', get_template_directory_uri() . '/js/assets/vendor/selectivizr-min.js', array(), '20141010', true );
    wp_enqueue_script( 'calcheight3', get_template_directory_uri() . '/js/assets/vendor/calcheight.js', array(), '20141010', true );
    wp_enqueue_script( 'easing1', get_template_directory_uri() . '/js/assets/vendor/jquery.easing.1.3.js', array(), '20141010', true );
    wp_enqueue_script( 'skitter4', get_template_directory_uri() . '/js/assets/vendor/jquery.skitter.js', array(), '20141010', true );
    wp_enqueue_script( 'responsiveslides6', get_template_directory_uri() . '/js/assets/vendor/responsiveslides.min.js', array(), '20141010', true );
    wp_enqueue_script( 'webplugin', get_template_directory_uri() . '/js/assets/plugins.js', array(), '20141010', true );

  }
  add_action('wp_enqueue_scripts','include_scripts_styles');
  function theme_prefix_setup() {
  	add_theme_support( 'custom-logo', array(
  		'height'      => 100,
  		'width'       => 400,
  		'flex-width' => true,
  	) );
  }
  add_action( 'after_setup_theme', 'theme_prefix_setup' );
  function register_menus(){
    register_nav_menus( array(
  		'top'    => __( 'Top Menu', 'top' ),
      'siderbar' => __('Side Menu', 'side'),
  		'bottom' => __( 'Bottom Menu', 'bottom' ),
  	) );
  }
  add_action('init','register_menus');
  add_theme_support('post-thumbnails');
  function add_featured_galleries_to_ctp() {
    $post_types = array();
    $id = get_the_ID();
    $frontpage_id = get_option( 'page_on_front' );
    if($id == $frontpage_id){
      $post_types = array('page');
      array_push($post_types, 'custom_post_type');
    }
  return $post_types;
}
add_theme_support('woocommerce');
add_filter('fg_post_types', 'add_featured_galleries_to_ctp' );
function remove_storefront_sidebar() {
    if ( is_archive() ) {
            remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );
    }
}
add_action( 'get_header', 'remove_storefront_sidebar' );
 ?>
