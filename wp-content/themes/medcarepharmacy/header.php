<!DOCTYPE html>
<html class="no-js">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="viewport" content="">
		<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
		<meta name="description" content="">
		<meta name="keywords" content="">

		<title></title>

		<link href="https://fonts.googleapis.com/css?family=Bitter" rel="stylesheet">
		<?php
      wp_head();
     ?>
	</head>

	<body <?php body_class(); ?>>
		<div class="protect-me">
      <header>
      	<div class="container clearfix">
      		<div class="logo-wrapper float-left">
      			<a href="index.php" class="cb-logo">
              <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
              $image = wp_get_attachment_image_src( $custom_logo_id , 'full' ); ?>
      				<img src="<?php echo $image[0]; ?>" alt="Company Logo"/>
      			</a>
      		</div>

      		<div class="contactinfo">
      			<div id="google_translate_element"></div>
      			<div class="contact-description">
      					<h4>For more information, please call:<span>240-464-1950</span></h4>
      			</div>
      		</div>
      		<a class="nav-toggle-button">
      			<h3 class="float-left">MENU</h3>
      				<i class="fa fa-navicon fa-2x">
      					&nbsp;&nbsp;&nbsp;&nbsp;
      				</i>
      		</a>
      	</div>
      </header>
			<nav id="main-nav">
				<div class="container">
					<div class="page-nav dropdown">
						<?php
								wp_nav_menu(array('theme_location' => 'top'));
						 ?>
					</div>
				</div>
			</nav>
