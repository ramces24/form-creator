$(document).ready(function() {
  $(".draggable-item").draggable({
    helper: 'clone'
 });


 droppable();
 $('.add-col-1').click(function(){
    $('.drop-container').append("<div class  = 'row'><div class = 'col-md-12 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 })
 $('.add-col-2').click(function(){
    $('.drop-container').append("<div class  = 'row'><div class = 'col-md-6 droppable'></div><div class = 'col-md-6 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 })
 $('.add-col-8-4').click(function(){
    $('.drop-container').append("<div class = 'row'><div class = 'col-md-8 droppable'></div><div class = 'col-md-4 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 })
 $('.add-col-4-8').click(function(){
    $('.drop-container').append("<div class = 'row'><div class = 'col-md-4 droppable'></div><div class = 'col-md-8 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 });
 $('.add-col-3-9').click(function(){
    $('.drop-container').append("<div class = 'row'><div class = 'col-md-3 droppable'></div><div class = 'col-md-9 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 });
 $('.add-col-9-3').click(function(){
    $('.drop-container').append("<div class = 'row'><div class = 'col-md-9 droppable'></div><div class = 'col-md-3 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 });
 $('.add-col-4-4-4').click(function(){
    $('.drop-container').append("<div class = 'row'><div class = 'col-md-4 droppable'></div><div class = 'col-md-4 droppable'></div><div class = 'col-md-4 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 });
 $('.add-col-3-3-3-3').click(function(){
    $('.drop-container').append("<div class = 'row'><div class = 'col-md-3 droppable vcenter'></div><div class = 'col-md-3 droppable'></div><div class = 'col-md-3 droppable'></div><div class = 'col-md-3 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 });
 $('.add-col-12').click(function(){
    $('.drop-container').append("<div class = 'row'><div class = 'col-md-1 droppable vcenter'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><div class = 'col-md-1 droppable'></div><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a></div>");
    droppable();
 })

});
    var id = 1;
    function droppable(){

      $(".droppable").droppable({
         accept: ".draggable-item",
         drop: function(event,ui){
             var type = $(ui.draggable).attr('id');
             if(type == 'button'){
               $(this).append("<div class = 'col-md-12'><button class = 'btn btn-width' id = '"+'button'+id+"'>button</button> <a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a> <a href = 'javascript:;' class = 'btn btn-info contentsettings'><i class='fa fa-cog' aria-hidden='true'></i></a></div>");
               id++;
             }
             if(type == 'textbox'){
               $(this).append("<div class = 'col-md-12'><textarea class = 'form-control' id = '"+'textarea'+id+"' placeholder = 'Input Text'></textarea><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a> <a href = 'javascript:;' class = 'btn btn-info contentsettings'><i class='fa fa-cog' aria-hidden='true'></i></a></div>");
               id++;
             }
             if(type == 'input-text'){
               $(this).append("<div class = 'col-md-12'><input type = 'text' class = 'form-control' id = '"+'text'+id+"'><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a> <a href = 'javascript:;' class = 'btn btn-info contentsettings'><i class='fa fa-cog' aria-hidden='true'></i></a></div>");
               id++;
             }
             if(type == 'select'){
               $(this).append("<div class = 'col-md-12'><select class = 'form-control' id = '"+'select'+id+"'><option value = 'sampleoption'>Sample Option</option></select><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a> <a href = 'javascript:;' class = 'btn btn-info contentsettings'><i class='fa fa-cog' aria-hidden='true'></i></a></div>");
               id++;
             }
             if(type == 'input-radio'){
               $(this).append("<div class = 'col-md-12 radiomar radio'><input type = 'radio' id = 'radio"+id+"' class = 'form-control ' value = 'Radio Button'><label>Radio Button</label><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a> <a href = 'javascript:;' class = 'btn btn-info contentsettings'><i class='fa fa-cog' aria-hidden='true'></i></a></div>");
               id++;
             }
             if(type == 'input-checkbox'){
               $(this).append("<div class = 'col-md-12 radiomar checkbox'><input type = 'checkbox' id = 'checkbox"+id+"' class = 'form-control ' value = 'Checkbox'><label>Checkbox</label><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a> <a href = 'javascript:;' class = 'btn btn-info contentsettings'><i class='fa fa-cog' aria-hidden='true'></i></a></div>");
               id++;
             }
             if(type == 'text-label'){
               $(this).append("<div class = 'col-md-12'><label id = 'label"+id+"'>Add Text Here</label><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a> <a href = 'javascript:;' class = 'btn btn-info contentsettings'><i class='fa fa-cog' aria-hidden='true'></i></a></div>");
               id++;
             }
             if(type == 'par_text'){
               $(this).append("<div class = 'col-md-12'><p id = 'par"+id+"'>Add Text Here</p><a href = 'javascript:;' class = 'btn btn-danger remove-col'><i class='fa fa-times' aria-hidden='true'></i></a> <a href = 'javascript:;' class = 'btn btn-info contentsettings'><i class='fa fa-cog' aria-hidden='true'></i></a></div>");
               id++;
             }
          }
      });
    }
    $(document).ready(function(){
      $(document).on('click','.remove-col',function(){
        var result = confirm("Are you sure?");
          if (result == true) {
              $(this).parent('div').remove();
          } else {}
      })
      $(document).on('click','.removeoptionval',function(){
        var result = confirm("Are you sure?");
          if (result == true) {
              $(this).parent('div').parent('div').remove();
          } else {}
      })
      $(document).on('click','.addoption',function(){
        $('#optionvaluelist').append('<div class = "row"><div class = "col-md-5"><input type = "text" value = "New Option" class = "form-control label-options"></div><div class = "col-md-5"><input type = "text" value = "New Option" class = "form-control value-options"></div><div class = "col-md-2"><a href = "javascript:;" class = "removeoptionval"><i class = "fa fa-minus-circle"></i> remove</a></div></div>');
      })
      $(document).on('click','.contentsettings',function(){
        var elementid = $(this).siblings().eq(0).attr('id');
        var tagName =   $(this).siblings().eq(0).prop('tagName');
        var type =   $(this).siblings().eq(0).attr('type');
        console.log(tagName);
        if((tagName == 'INPUT' && type == 'text') || (tagName == 'INPUT' && type == 'password') || (tagName == 'INPUT' && type == 'email')){
          $('#inputmodal').modal('show');
          $('#inputplaceholder').val($('#'+elementid).attr('placeholder'));
          $('#inputvalue').val($('#'+elementid).val());
          $('#inputclass').val($('#'+elementid).attr('class'));
          $('#inputselect').val($('#'+elementid).attr('type'));
          $('#saveinputsettings').attr('specificinputid',elementid);
        }
        if(tagName == 'LABEL'){
          $('#labelmodal').modal('show');
          $('#labelname').val($('#'+elementid).text());
          $('#savelabelsettings').attr('specificlabelid',elementid);

        }
        // i-edit rani naku soon
        if(tagName == 'P'){
          $('#partextmodal').modal('show');
          $('#partextlabelname').val($('#'+elementid).text());
          $('#savepartextsettings').attr('specificpartextid',elementid);
        }
        if(tagName == 'BUTTON'){
          $('#buttonmodal').modal('show');
          $('#buttonval').val($('#'+elementid).text());
          $('#buttonname').val($('#'+elementid).attr('name'));
          $('#buttonclass').val($('#'+elementid).attr('class'));
          $('#savebuttonsettings').attr('specificbuttonid',elementid);
        }
        if(tagName == 'TEXTAREA'){
          $('#textareadefaultval').val($('#'+elementid).val());
          $('#textareaname').val($('#'+elementid).attr('name'));
          $('#textareaplaceholder').val($('#'+elementid).attr('placeholder'));
          $('#textareaclass').val($('#'+elementid).attr('class'));
          $('#textareamodal').modal('show');
          $('#savetextboxsettings').attr('specificbuttonid',elementid);
        }
        if(tagName == 'SELECT'){
            var disabledvalue = "";
            var values = $("#"+elementid+">option").map(function(){
              if($(this).is(':enabled')){
                return $(this).val();
              }
            });
            var labels = $("#"+elementid+">option").map(function(){
              if($(this).is(':enabled')){
                return $(this).html();
              }
              else{
                return disabledvalue = $(this).text();
              }
            });
            var appendoptionvalue = "";
            for(var x = 0 ; x < values.length;x++){
                appendoptionvalue += "<div class = 'row'>";
                appendoptionvalue += "<div class = 'col-md-5'><input type = 'text' class ='form-control label-options' value = '"+labels[x]+"'></div><div class = 'col-md-5'><input type = 'text' class ='form-control value-options' value = '"+values[x]+"'></div>";
                if(x >= 1){
                  appendoptionvalue += "<div class = 'col-md-2'><a href = 'javascript:;' class = 'removeoptionval'><i class = 'fa fa-minus-circle'></i> remove</a></div>";
                }
                appendoptionvalue += "</div>";
            }
            $('#optionvaluelist').children().remove();
            $('#optionvaluelist').append(appendoptionvalue);
            $('#selectmodal').modal('show');
            $('#saveselectsettings').attr('specificselectid',elementid);
            $('#selectclass').val($('#'+elementid).attr('class'));
            $('#selectplaceholder').val(disabledvalue);
            $('#selectname').val($('#'+elementid).attr('name'));
        }
        if(type == 'radio'){
          $('#radiomodal').modal('show');
          $('#radioname').val($('#'+elementid).attr('name'));
          $('#radioclass').val($('#'+elementid).attr('class'));
          $('#radiovalue').val($('#'+elementid).val());
          $('#radioplaceholder').val($(this).siblings().eq(1).text());
          $('#saveradiosettings').attr('specificradiotid',elementid);

        }
        if(type == 'checkbox'){
          $('#checkboxmodal').modal('show');
          $('#checkboxname').val($('#'+elementid).attr('name'));
          $('#checkboxclass').val($('#'+elementid).attr('class'));
          $('#checkboxvalue').val($('#'+elementid).val());
          $('#checkboxplaceholder').val($(this).siblings().eq(1).text());
          $('#savecheckboxsettings').attr('specificcheckboxtid',elementid);

        }
      })
      $('#savecheckboxsettings').click(function(){
       var targetspecificcheckbox = $(this).attr('specificcheckboxtid');
       $('#'+targetspecificcheckbox).attr('name',$('#checkboxname').val());
       $('#'+targetspecificcheckbox).val($('#checkboxvalue').val());
       $('#'+targetspecificcheckbox).attr('class',$('#checkboxclass').val());
       $('#'+targetspecificcheckbox).siblings().eq(0).text($('#checkboxplaceholder').val());
      })
      $('#saveradiosettings').click(function(){
        var targetspecificradio = $(this).attr('specificradiotid');
        $('#'+targetspecificradio).attr('name',$('#radioname').val());
        $('#'+targetspecificradio).val($('#radiovalue').val());
        $('#'+targetspecificradio).attr('class',$('#radioclass').val());
        $('#'+targetspecificradio).siblings().eq(0).text($('#radioplaceholder').val());
      })
      $('#savebuttonsettings').click(function(){
          var targetspecificbtn = $(this).attr('specificbuttonid');
          $('#'+targetspecificbtn).text($('#buttonval').val());
          $('#'+targetspecificbtn).attr('name',$('#buttonname').val());
          $('#'+targetspecificbtn).attr('class',$('#buttonclass').val());
      })
      $('#saveselectsettings').click(function(){
        var targetselect = $(this).attr('specificselectid');
        var label = $('.label-options').map(function(){
          return $(this).val();
        })
        var value = $('.value-options').map(function(){
          return $(this).val();
        })
        if(label.length == value.length){
          var optionlength = label.length;
          $('#'+targetselect+" option").remove();
          for(var i = 0; i < optionlength;i++){
              $('#'+targetselect).append("<option value = '"+value[i]+"'>"+label[i]+"</option>");
          }
          if( $('#selectplaceholder').val() != "" ){
            $('#'+targetselect).append("<option disabled='true' selected>"+$('#selectplaceholder').val()+"</option>");
          }
            $('#'+targetselect).attr('name',$('#selectname').val());
        }
        else{
          alert('What are you doing ?');
        }
      })
      $('#savetextboxsettings').click(function(){
        var targettextbox = $(this).attr('specificbuttonid');
        $('#'+targettextbox).val($('#textareadefaultval').val());
        $('#'+targettextbox).attr('name',$('#textareaname').val());
        $('#'+targettextbox).attr('class',$('#textareaclass').val());
        $('#'+targettextbox).attr('placeholder',$('#textareaplaceholder').val());
      })
      $('#savelabelsettings').click(function(){
        var targettextbox = $(this).attr('specificlabelid');
        $('#'+targettextbox).text($('#labelname').val());
      })
      $('#saveinputsettings').click(function(){
        var targetinput = $(this).attr('specificinputid');
        $('#'+targetinput).val($('#inputvalue').val());
        $('#'+targetinput).attr('placeholder',$('#inputplaceholder').val());
        $('#'+targetinput).attr('type',$('#inputselect').val());
        $('#'+targetinput).attr('name',$('#inputname').val());
        $('#'+targetinput).attr('class',$('#inputclass').val());
        $('#'+targetinput).attr('btnstyle',$('input[name="input-style"]:checked').val());
        //$('#'+targetinput).addClass($('input[name="input-style"]:checked').val());
      })
      $('#gethtmlcode').click(function(){
        $('#removehtmltags').children().remove();
        $('#htmlcontainermodal').modal('show');
        var temphtmltags = $('#htmlcontainer').html();
        $('#removehtmltags').append(temphtmltags);
        $('#removehtmltags .contentsettings').remove();
        $('#removehtmltags .remove-col').remove();
        $('#removehtmltags .droppable').removeClass('droppable');
        $('#removehtmltags .vcenter').removeClass('vcenter');
        $('#removehtmltags .ui-droppable').removeClass('ui-droppable');
        var htmltags  = $('#removehtmltags').html();
        $('#htmlcodes').val("<html><head><link rel='stylesheet' href ='css/form-bootstrap.min.css'><title></title></head><body><form method='post' id='submitform'><div class = 'container wd-auto'>"+htmltags+"</div></form></body></html>");
        // beautify html codes
        code=$("#htmlcodes").eq(0);
        val=$.replace_tag(code.minify());
        el=$("<div></div>").html(val);
        $.prettify_code(el);
        code.css('white-space', 'pre').show_code($.undo_tag(el.html()));
        //end
      })
      $('#savefile').click(function(){
          $('#save-file').modal('show');
          var htmlcss = $('#htmlcodes').val();
          $('.htmlcodestodownload').val(htmlcss);
     })
      $('.viewforms').click(function(){
           var form_id = $(this).attr('id');
           $.ajax({
                url:ajaxurl,
                type:"POST",
                data:{action:"get_form",form_id:form_id},
                success:function(data){
                    $('#table-forms .modal-body #html_output').html(data);
                    $('#table-forms .modal-body #html_codes').text(data);
                    $('#table-forms .modal-body #html_codes').css('white-space', 'pre');
                }
           })
          $('#downloadfilename').val($(this).attr('frmname'));
          $('#table-forms').modal('show');
      })
      $('.deleteforms').click(function(){
          var form_id = $(this).attr('id');
          $.ajax({
               url:ajaxurl,
               type:"POST",
               data:{action:"delete_form",form_id:form_id},
               success:function(data){
                   location.reload();
               }
          })
     //      $.ajax({
     //           url:ajaxurl,
     //           type:"POST",
     //           data:{action:"get_form",form_id:form_id},
     //           success:function(data){
     //               $('#table-forms .modal-body #html_output').html(data);
     //               $('#table-forms .modal-body #html_codes').text(data);
     //               $('#table-forms .modal-body #html_codes').css('white-space', 'pre');
     //           }
     //      })
     //     $('#downloadfilename').val($(this).attr('frmname'));
     //     $('#table-forms').modal('show');
     })
     $('.htmlcde').click(function(){
          $('.copy-html').show();
          $('.dwnld').hide();
     })
     $('.htmlotpt').click(function(){
          $('.copy-html').hide();
          $('.dwnld').show();
     })
     $('.copy-html').click(function(){
          window.getSelection().selectAllChildren( document.getElementById('html_codes') );
          document.execCommand("Copy");
     })
      $('#gettype').click(function(){
        $('#download-type').modal('show');
      })
      $('#download-now').click(function(){
        var link = host+"css/form-bootstrap.min.css";
        var link2 = host+"css/adminLTE.min.css";
        var bootstrap  = "";
        var code = document.getElementById('htmlcodes').value;
        var savetype = $('#downloadfiletype').val();
        var savename = $('#downloadfilename').val();
        var zip = new JSZip();
        $.ajax({
          url:link,
          type:"GET",
          success:function(data){
            $.ajax({
              url:link2,
              type:"GET",
              success:function(data1){
                  zip.file(savename+"."+savetype, code);
                  zip.file("css/adminLTE.min.css", data1);
                  zip.file("css/form-bootstrap.min.css", data);
                  zip.generateAsync({type:"blob"})
                  .then(function(content) {
                      saveAs(content, savename+".zip");
                  });
              }
            })
          }
        })
      })
      $('#download-now-btn').click(function(){
        var link = host+"css/form-bootstrap.min.css";
        var link2 = host+"css/adminLTE.min.css";
        var bootstrap  = "";
        var code = document.getElementById('html_codes').value;
        var savetype = $('#downloadfiletype').val();
        var savename = $('#downloadfilename').val();
        var zip = new JSZip();
        $.ajax({
          url:link,
          type:"GET",
          success:function(data){
            $.ajax({
              url:link2,
              type:"GET",
              success:function(data1){
                  zip.file(savename+"."+savetype, code);
                  zip.file("css/adminLTE.min.css", data1);
                  zip.file("css/form-bootstrap.min.css", data);
                  zip.generateAsync({type:"blob"})
                  .then(function(content) {
                      saveAs(content, savename+".zip");
                  });
              }
            })
          }
        })
      })
      $('#download-now-btn2').click(function(){
        var link = host+"css/form-bootstrap.min.css";
        var link2 = host+"css/adminLTE.min.css";
        var bootstrap  = "";
        var code = $('#html_codes').text();
        var savetype = $('#downloadfiletype').val();
        var savename = $('#downloadfilename').val();
        var zip = new JSZip();
        $.ajax({
          url:link,
          type:"GET",
          success:function(data){
            $.ajax({
              url:link2,
              type:"GET",
              success:function(data1){
                  zip.file(savename+"."+savetype, code);
                  zip.file("css/adminLTE.min.css", data1);
                  zip.file("css/form-bootstrap.min.css", data);
                  zip.generateAsync({type:"blob"})
                  .then(function(content) {
                      saveAs(content, savename+".zip");
                  });
              }
            })
          }
        })
      })
      $('#savefilename').keyup(function(){
           var txt = $(this).val();
           var new_txt = txt.replace(" ","_");
           var formname=new_txt.toLowerCase();
           $(this).val(formname);
      })
       $('#savedbform').submit(function(e){
          e.preventDefault();
          var formdata = $(this).serialize()+"&action=save_new_form";
          var data =
          $.ajax({
            url:ajaxurl,
            type:"POST",
            data:formdata,
            success:function(data){
              $('.message_notif').show();
              $('#save-file').modal('hide');
              $('#htmlcontainer').children().remove();
              // location.reload();
            }
          })
       })

      function saveTextAsFile(type,name){
           var textToWrite = document.getElementById('htmlcodes').value;
           var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
           var fileNameToSaveAs = name+"."+type;
           var downloadLink = document.createElement("a");
           downloadLink.download = fileNameToSaveAs;
           downloadLink.innerHTML = "Download File";
           if (window.webkitURL != null)
           {
               downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
           }
           else
           {
               // Firefox requires the link to be added to the DOM
               // before it can be clicked.
               downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
               //downloadLink.onclick = destroyClickedElement;
               downloadLink.style.display = "none";
               document.body.appendChild(downloadLink);
           }

           downloadLink.click();
       }
       $('.drop-inputs h4.grdsys').on("click",function(){
          $('.dropdownmn').slideToggle(1000, function(){
               $('dropdownmn ul').show(100);
          });
       });
    })
    // $(document).ready(function(){
    //   var zip = new JSZip();
    //   zip.file("hello.txt", "Hello[p my)6cxsw2q");
    //   // oops, cat on keyboard. Fixing !
    //   zip.file("hello.txt", "Hello World\n");
    //
    //   // create a file and a folder
    //   zip.file("nested/hello.txt", "Hello World\n");
    //   // same as
    //   zip.folder("nested").file("hello.txt", "Hello World\n");
    //   zip.generateAsync({type:"blob"})
    //   .then(function(content) {
    //       // see FileSaver.js
    //       saveAs(content, "example.zip");
    //   });
    // })
    /*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */
var saveAs=saveAs||function(e){"use strict";if(typeof e==="undefined"||typeof navigator!=="undefined"&&/MSIE [1-9]\./.test(navigator.userAgent)){return}var t=e.document,n=function(){return e.URL||e.webkitURL||e},r=t.createElementNS("http://www.w3.org/1999/xhtml","a"),o="download"in r,a=function(e){var t=new MouseEvent("click");e.dispatchEvent(t)},i=/constructor/i.test(e.HTMLElement)||e.safari,f=/CriOS\/[\d]+/.test(navigator.userAgent),u=function(t){(e.setImmediate||e.setTimeout)(function(){throw t},0)},s="application/octet-stream",d=1e3*40,c=function(e){var t=function(){if(typeof e==="string"){n().revokeObjectURL(e)}else{e.remove()}};setTimeout(t,d)},l=function(e,t,n){t=[].concat(t);var r=t.length;while(r--){var o=e["on"+t[r]];if(typeof o==="function"){try{o.call(e,n||e)}catch(a){u(a)}}}},p=function(e){if(/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(e.type)){return new Blob([String.fromCharCode(65279),e],{type:e.type})}return e},v=function(t,u,d){if(!d){t=p(t)}var v=this,w=t.type,m=w===s,y,h=function(){l(v,"writestart progress write writeend".split(" "))},S=function(){if((f||m&&i)&&e.FileReader){var r=new FileReader;r.onloadend=function(){var t=f?r.result:r.result.replace(/^data:[^;]*;/,"data:attachment/file;");var n=e.open(t,"_blank");if(!n)e.location.href=t;t=undefined;v.readyState=v.DONE;h()};r.readAsDataURL(t);v.readyState=v.INIT;return}if(!y){y=n().createObjectURL(t)}if(m){e.location.href=y}else{var o=e.open(y,"_blank");if(!o){e.location.href=y}}v.readyState=v.DONE;h();c(y)};v.readyState=v.INIT;if(o){y=n().createObjectURL(t);setTimeout(function(){r.href=y;r.download=u;a(r);h();c(y);v.readyState=v.DONE});return}S()},w=v.prototype,m=function(e,t,n){return new v(e,t||e.name||"download",n)};if(typeof navigator!=="undefined"&&navigator.msSaveOrOpenBlob){return function(e,t,n){t=t||e.name||"download";if(!n){e=p(e)}return navigator.msSaveOrOpenBlob(e,t)}}w.abort=function(){};w.readyState=w.INIT=0;w.WRITING=1;w.DONE=2;w.error=w.onwritestart=w.onprogress=w.onwrite=w.onabort=w.onerror=w.onwriteend=null;return m}(typeof self!=="undefined"&&self||typeof window!=="undefined"&&window||this.content);if(typeof module!=="undefined"&&module.exports){module.exports.saveAs=saveAs}else if(typeof define!=="undefined"&&define!==null&&define.amd!==null){define("FileSaver.js",function(){return saveAs})}
