
  <script>
    var host = "<?php echo $urldir; ?>";
  </script>
  <div class="container" style = 'padding-top:10px'>
    <div class="message_notif" style="display:none">
      <div id="message" class="updated notice notice-success is-dismissible"><p>New form created.</p></div>
    </div>
      <div class="row padding">
          <div class="col-md-9">
            <div class="row drop-inputs drop-container" id = "htmlcontainer">

            </div>
          </div>
          <div class="col-md-3">
            <div class="row drop-inputs" style="min-height:auto!important;position: fixed;width: 18%;background: #efefef;">
              <h4 class="grdsys">GRID SYSTEM&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>
</h4>
              <div class="dropdownmn">
              <ul class="nav nav-list">
                <!-- <li class="nav-header"> <h4>GRID SYSTEM</h4></li> -->
                <li>
                  <div class="form-group add-col-1">
                    <a href="javascript:;">
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/12.jpg" alt="12"/>
                    </a>
                  </div>
                </li>
                <li>
                  <div class="form-group add-col-2">
                    <a href="javascript:;">
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/66.jpg" alt="66"/>
                    </a>
                  </div>
                </li>
                <li>
                  <div class="form-group add-col-8-4">
                    <a href="javascript:;">
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/84.jpg" alt="84"/>
                    </a>
                  </div>
                </li>
                <li>
                  <div class="form-group add-col-4-8">
                    <a href="javascript:;">
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/48.jpg" alt="84"/>
                    </a>
                  </div>
                </li>
                <li>
                  <div class="form-group add-col-3-9">
                    <a href="javascript:;">
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/39.jpg" alt="39"/>
                    </a>
                  </div>
                </li>
                <li>
                  <div class="form-group add-col-9-3">
                    <a href="javascript:;">
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/93.jpg" alt="93"/>
                    </a>
                  </div>
                </li>
                <li>
                  <div class="form-group add-col-4-4-4">
                    <a href="javascript:;">
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/444.jpg" alt="444"/>
                    </a>
                  </div>
                </li>
                <li>
                  <div class="form-group add-col-3-3-3-3">
                    <a href="javascript:;" >
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/3333.jpg" alt="3333"/>
                    </a>
                  </div>
                </li>
                <li>
                  <div class="form-group add-col-12">
                    <a href="javascript:;" >
                         <img class="grid-pop" src="<?php echo $urldir; ?>images/3333.jpg" alt="3333"/>
                    </a>
                  </div>
                </li>
              </ul>
            </div>
                <h4>HTML Elements</h4>
              <div class="row html-elements">
                <div class="col-md-4">
                  <div class="form-group draggable-item" id = "button">
                   <a class="btn btn-app">
                      <i class="fa fa-location-arrow"></i> Button
                   </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group draggable-item" id = "select">
                    <a class="btn btn-app">
                      <i class="fa fa-arrow-circle-o-down"></i> Select
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group draggable-item" id = "textbox">
                    <a class="btn btn-app">
                      <i class="fa fa-file-text-o"></i> Text Area
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group draggable-item" id = "input-text">
                    <a class="btn btn-app">
                      <i class="fa fa-text-width"></i> Input
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group draggable-item" id = "input-radio">
                    <a class="btn btn-app">
                      <i class="fa fa-dot-circle-o"></i> Radio Button
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group draggable-item" id = "input-checkbox">
                    <a class="btn btn-app">
                      <i class="fa fa-check-square-o"></i> Checkbox
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group draggable-item" id = "text-label">
                    <a class="btn btn-app">
                      <i class="fa fa-font"></i> Text
                    </a>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group draggable-item" id = "par_text">
                    <a class="btn btn-app">
                      <i class="fa fa-align-justify" aria-hidden="true"></i> Paragraph Text
                    </a>
                  </div>
                </div>
                <div class="col-md-12">
                    <button type="button" class="btn btn-info" name="button" id = "gethtmlcode"><i class="icon-compass"></i> Get HTML Code </button>
                </div>
              </div>

            </div>
          </div>
      </div>
  </div>

  <!-- input modal -->
  <div class="modal fade" id="inputmodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Input Tag</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Field Name</label>
              <input type="text" name="" value="" class = 'form-control' id = "inputname">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Placeholder</label>
              <input type="text" name="" value="" class = 'form-control' id = "inputplaceholder">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Value</label>
              <input type="text" name="" value="" class = 'form-control' id = "inputvalue">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Type</label>
              <select class="form-control" name="" id = "inputselect">
                  <option value="text">Text</option>
                  <option value="password">Password</option>
                  <option value="email">Email</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Class</label>
              <input type="text" name="" value="" class = 'form-control' id = "inputclass">
            </div>
          </div>
          <div class="form-group">
             <div class="col-md-6">
               <label for="">Input Style</label>
               <ul class="input-style">
                    <li><input type="radio" name="input-style" value="border-bottom"> Border Bottom</li>
                    <li><input type="radio" name="input-style" value="full-border"> Full Border</li>
               <ul>
             </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id = "saveinputsettings" class="btn btn-success" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <!-- label modal -->
  <div class="modal fade" id="labelmodal" role="dialog">
   <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Label Tag</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Text Label</label>
              <input type="text" name="" value="" class = 'form-control' id = "labelname">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id = "savelabelsettings" class="btn btn-success" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

   </div>
  </div>
  <!-- end modal -->

  <!-- label modal -->
  <div class="modal fade" id="partextmodal" role="dialog">
   <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Paragraph Text</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Text Label</label>
              <input type="text" name="" value="" class = 'form-control' id = "partextlabelname">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id = "savepartextsettings" class="btn btn-success" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

   </div>
  </div>
  <!-- end modal -->

  <!-- input checkbox m -->
  <div class="modal fade" id="checkboxmodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Checkbox Tag</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Field Name</label>
              <input type="text" name="" value="" class = 'form-control' id = "checkboxname">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Class</label>
              <input type="text" name="" value="" class = 'form-control' id = "checkboxclass">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Value</label>
              <input type="text" name="" value="" class = 'form-control' id = "checkboxvalue">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Text Label</label>
              <input type="text" name=""  class = 'form-control' id = "checkboxplaceholder">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id = "savecheckboxsettings" class="btn btn-success" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <!-- input radio m -->
  <div class="modal fade" id="radiomodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Radio Tag</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Field Name</label>
              <input type="text" name="" value="" class = 'form-control' id = "radioname">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Class</label>
              <input type="text" name="" value="" class = 'form-control' id = "radioclass">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Value</label>
              <input type="text" name="" value="" class = 'form-control' id = "radiovalue">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Text Label</label>
              <input type="text" name=""  class = 'form-control' id = "radioplaceholder">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" id = "saveradiosettings" class="btn btn-success" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->


  <!-- input modal -->
  <div class="modal fade" id="buttonmodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Button</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <label for="">Text Label</label>
              <input type="text" name="" value="" class = 'form-control' id = 'buttonval'>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Class</label>
              <input type="text" name="" value="" class = 'form-control' id = 'buttonclass'>
            </div>
            <div class="col-md-6">
              <label for="">Field Name</label>
              <input type="text" name="" value="" class = 'form-control' id = "buttonname">
            </div>
          </div>
        </div>
        <br/>
        <div class="modal-footer">
          <button type="button" id = "savebuttonsettings" class="btn btn-success" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <!-- input modal -->
  <div class="modal fade" id="textareamodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Text Area</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Field Name</label>
              <input type="text" name="" value="" class = 'form-control' id = 'textareaname'>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <label for="">Default Value</label>
              <input type="text" name="" value="" class = 'form-control' id = 'textareadefaultval'>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Class</label>
              <input type="text" name="" value="" class = 'form-control' id = 'textareaclass'>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Placeholder</label>
              <input type="text" name="" value="" class = 'form-control' id = 'textareaplaceholder'>
            </div>
          </div>
        </div>
        <br/>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id = "savetextboxsettings" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <!-- input modal -->
  <div class="modal fade" id="selectmodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <label for="">Field Name</label>
              <input type="text" name="" value="" class = 'form-control' id = "selectname">
          </div>
          <div class="form-group">
            <div class="col-md-6">
              <label for="">Class</label>
              <input type="text" name="" value="" class = 'form-control' id = "selectclass">
            </div>
            <div class="col-md-6">
              <label for="">Placeholder</label>
              <input type="text" name="" value="" class = 'form-control' id = "selectplaceholder">
            </div>
          </div>
          <div class="form-group" style = 'margin:59px 0'>
              <div class="col-md-12">
                  <label for="">Options</label>&nbsp &nbsp<a href="javascript:;" class="addoption"><i class = 'fa fa-plus-circle'></i></a>
              </div>
              <div class="col-md-5">
                  <label for="">Text Label</label>
              </div>
              <div class="col-md-5">
                  <label for="">Value</label>
              </div>
              <div class = 'form-group' id="optionvaluelist">
              </div>
          </div>
        </div>
        <br/>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" id = "saveselectsettings" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <!-- input modal -->
  <div class="modal fade" id="htmlcontainermodal" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">HTML CODE</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <textarea name="name" id = "htmlcodes"></textarea>
          </div>
        </div>
        <br/>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" id = "gettype" data-dismiss="modal"><i class = 'fa fa-download'></i> Download</button> <button type="button" class="btn btn-success" id = "savefile" data-dismiss="modal">Save</button><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <!-- input modal -->
  <div class="modal fade" id="download-type" role="dialog">
    <div class="modal-dialog modal-sm">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">HTML CODE</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <label for="">File Name</label>
              <input type="text" name=""  placeholder = "Filename" class="form-control" id = "downloadfilename">
          </div>
          <div class="form-group">
              <label for="">File Type :</label>
              <select class="form-control" name="" id = "downloadfiletype">
                  <option value="html">.html</option>
                  <option value="docx">.docx</option>
                  <option value="php">.php</option>
                  <option value="txt">.txt</option>
              </select>
          </div>
        </div>
        <br/>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" id = "download-now" data-dismiss="modal"><i class = 'fa fa-download'></i></button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <!-- input modal -->
  <div class="modal fade" id="save-file" role="dialog">
    <div class="modal-dialog modal-sm">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">SAVE FILE TO DATABASE</h4>
        </div>
        <form id="savedbform" action="" method="post">
          <textarea hidden class="htmlcodestodownload" name="name" rows="8" cols="80"></textarea>
          <div class="modal-body">
            <div class="form-group">
                <label for="">Form Name</label>
                <input type="text" name="form_name" required  placeholder = "Filename" class="form-control frm-name" id = "savefilename">
            </div>
            <div class="form-group">
                <label for="">Created By</label>
                <input type="text" name="created_by" required placeholder = "Filename" class="form-control" id = "savefilename">
            </div>
          </div>
          <br/>
          <div class="modal-footer">
            <button type="submit" class="btn btn-info"><i class="fa fa-floppy-o" aria-hidden="true"></i></button>
          </div>
        </form>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <div id="removehtmltags" style = "display:none">


  </div>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- </body>
</html> -->
