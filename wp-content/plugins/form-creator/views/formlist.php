<script>
  var host = "<?php echo $urldir; ?>";
</script>
<div class="wrap">
  <p>Form Lists</p>
  <table class="table table-hover table-bordered " id="example">
     <thead style="background-color:#21343e;color:#fff;">
          <tr>
               <th>Form Name</th>
               <th>Date Created</th>
               <th>Created By</th>
               <th>Action</th>
          </tr>
     </thead>
     <tbody>
       <?php if(!empty($formlist)):  ?>
            <?php foreach ($formlist as $data): ?>
               <tr>
                   <td><?php echo $data->form_name; ?></td>
                   <td><?php echo $data->form_dt_created; ?></td>
                   <td><?php echo $data->form_created_by; ?></td>
                   <td><button type="button" class="btn btn-primary viewforms" frmname = "<?php echo $data->form_name; ?>" name="button" id = "<?php echo $data->form_id; ?>"><i class="fa fa-file-text"></i> View</button>&nbsp;<button type="button" class="btn btn-danger deleteforms"  name="button" id = "<?php echo $data->form_id; ?>"><i class="fa fa-times"></i> Delete</button></td>
               </tr>
            <?php endforeach; ?>
       <?php else: ?>
            <tr>
                 <td colspan="4" style="text-align:center">No Forms Created</td>
            </tr>
       <?php endif; ?>
     </tbody>
  </table>
</div>

<!-- input modal -->
  <div class="modal fade" id="table-forms" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Form Data</h4>
        </div>
        <div class="modal-body">
          <div role="tabpanel">
               <!-- Nav Tabs-->
               <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#html_output" aria-controls="html_ouput" role="tab" data-toggle="tab" class="htmlotpt">HTML Output</a></li>
                    <li role="presentation"><a class="htmlcde" href="#html_codes" aria-controls="html_codes" role="tab" data-toggle="tab">HTML Codes</a></li>
               </ul>
               <!-- Tab panes-->
               <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="html_output"></div>
                      <div role="tabpanel" class="tab-pane" id="html_codes"></div>
               </div>
          </div>
        </div>
        <br/>
        <div class="modal-footer">
             <button type="button" class="btn btn-primary dwnld" id = "gettype" data-dismiss="modal"><i class = 'fa fa-download'></i> Download</button>
             <button type="button" class="btn btn-success copy-html" style="display:none">Copy HTML Code</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- end modal -->

  <!-- input modal -->
  <div class="modal fade" id="download-type" role="dialog">
   <div class="modal-dialog modal-sm">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">HTML CODE</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <label for="">File Name</label>
              <input type="text" name=""  placeholder = "Filename" class="form-control" id = "downloadfilename">
          </div>
          <div class="form-group">
              <label for="">File Type :</label>
              <select class="form-control" name="" id = "downloadfiletype">
                  <option value="html">.html</option>
                  <option value="docx">.docx</option>
                  <option value="php">.php</option>
                  <option value="txt">.txt</option>
              </select>
          </div>
        </div>
        <br/>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" id = "download-now-btn2" data-dismiss="modal"><i class = 'fa fa-download'></i></button>
        </div>
      </div>

   </div>
  </div>
  <!-- end modal -->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.jsintegrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txacrossorigin="anonymous"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.cssintegrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4ucrossorigin="anonymous">
<link href="https://nightly.datatables.net/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<script src="https://nightly.datatables.net/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script type="text/javascript">
     $(document).ready(function(){
          $('#example').DataTable();
     })
</script>
