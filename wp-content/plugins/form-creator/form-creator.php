<?php
/*
  Plugin Name: Form Creator - SP Team
  Plugin URI:
  Description: This plugin is created by SP Team of Proweaver INC
  Version: 1.0
  Author: SP Team
*/

class WP_Form_Creator{
    private $model;
    private $table;
    function __construct() {
        global $wpdb;
        $this->model = $wpdb;
        $this->table = $this->model->prefix."sp_form_tbl";
        add_action('admin_enqueue_scripts',array( $this,'register_my_styles'));
        add_action( 'admin_menu', array( $this, 'wpa_add_menu' ));
        add_action( 'wp_ajax_save_new_form', array($this,'save_new_form'));
        add_action( 'wp_ajax_get_form', array($this,'get_form'));
        add_action( 'wp_ajax_delete_form', array($this,'delete_form'));
        register_activation_hook( __FILE__, array( $this, 'wpa_install' ) );
        register_deactivation_hook( __FILE__, array( $this, 'wpa_uninstall' ) );
    }
    function wpa_add_menu() {
        add_menu_page('Form Creator', 'Form-Creator','manage_options','form-creator',array($this,'formlist'),'dashicons-schedule',80);
        add_submenu_page( 'form-creator', 'New Form', 'Create Form', 'manage_options', 'add-form',array($this,'create_new_form'));
    }
    function create_new_form(){
      $this->view('create');
    }
    function save_new_form(){
      if(isset($_POST)){
        $this->model->insert($this->table,array("form_name" => $this->filter_post($_POST['form_name']), "form_created_by" => $_POST['created_by'], "form_html_body" => $_POST['name'], "form_dt_created" => date('Y-m-d H:i:s') ), array( "%s" ));
      }
      wp_die();
    }
    function get_form(){
         $form_id = $_POST['form_id'];
         $res = $this->model->get_results("SELECT * FROM $this->table WHERE form_id = $form_id");
         $str = $res[0]->form_html_body;
         $data = stripslashes_deep( $str );
         echo $data;
         wp_die();
    }
    function delete_form(){
         $form_id = $_POST['form_id'];
         $res = $this->model->delete($this->table, array('form_id'=>$form_id));
         wp_die();
    }
    function remove_filter($string){
      return wp_kses_stripslashes($string);
    }
    function formlist(){
      $data['formlist'] = $this->model->get_results("SELECT * FROM $this->table ORDER BY form_name ASC");
      $this->view('formlist',$data);
    }
    function view($name,$data = array()){
      $dir = plugin_dir_path( __FILE__ );
      $urldir = plugin_dir_url(__FILE__);
      if(!empty($data)){
        foreach ($data as $key => $value) {
          ${$key} = $value;
        }
      }
      $file = $dir.'/views/'. $name . '.php';
      include( $file );
    }
    function filter_post($post_value){
      $data = mysql_real_escape_string($post_value);
      $data = htmlspecialchars($data, ENT_IGNORE, 'utf-8');
      $data = strip_tags($data);
      $data = stripslashes($data);
      $data = trim($data);
      return $data;
    }
    function register_my_styles(){
        wp_enqueue_style( 'bootstrap', plugin_dir_url(__FILE__).'css/bootstrap.min.css' );
        wp_enqueue_style( 'font-awesome', plugin_dir_url(__FILE__).'font-awesome/css/font-awesome.min.css' );
        wp_enqueue_style( 'font-awesome', plugin_dir_url(__FILE__).'font-awesome/css/font-awesome.css' );
        wp_enqueue_style( 'adminLTE', plugin_dir_url(__FILE__).'css/adminLTE.min.css' );
        wp_enqueue_style( 'style', plugin_dir_url(__FILE__).'css/style.css' );
        wp_enqueue_script( 'jqueryscript', plugin_dir_url(__FILE__).'js/jquery.min.js' );
        wp_enqueue_script( 'bootstrapjs', plugin_dir_url(__FILE__).'js/bootstrap.min.js' );
        wp_enqueue_script( 'custom', plugin_dir_url(__FILE__).'js/customscript.js' );
        wp_enqueue_script( 'jzip', plugin_dir_url(__FILE__).'js/jszip.min.js' );
        wp_enqueue_script( 'lib', plugin_dir_url(__FILE__).'js/lib.js' );
        wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => 1234 ) );
    }

    /*
     * Actions perform on loading of menu pages
     */
    function wpa_page_file_path() {



    }
    function wpa_install() {
      $charset_collate = $this->model->get_charset_collate();
      $sql = "CREATE TABLE $this->table (
        form_id mediumint(9) NOT NULL AUTO_INCREMENT,
        form_dt_created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        form_name varchar(55)  NULL,
        form_html_body LONGTEXT  NULL,
        form_created_by varchar(55)  NULL,
        PRIMARY KEY  (form_id)
      ) $charset_collate;";
      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
      dbDelta( $sql );
    }
}
new WP_Form_Creator();
 ?>
